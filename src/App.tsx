import React from 'react';
import './App.css';
import List from './components/List'
import Filters from "./components/Filters";
import {CountriesContextProvider} from './context'

function App() {

  return (
    <div className="App">
      <CountriesContextProvider>
        <Filters/>
        <List/>
      </CountriesContextProvider>
    </div>
  );
}

export default App;
