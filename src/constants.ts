import {Country} from "./types";

export const PATH = 'https://restcountries.com/v2/all?fields=name,region,area'

export const EMPTY_LIST = [] as Country[]
export const EMPTY_PAGINATED_LIST = [[]] as Country[][]
export const LITHUANIA_AREA = 65300.0

export enum ORDER_DIR {
  ASC = 'ASC',
  DESC = 'DESC',
}

export enum FILTER_STATE {
  ALL = 'ALL',
  OCEANIA_COUNTRIES = 'OCEANIA COUNTRIES',
  SMALLER_LTH_AREA = 'SMALLER LITHUANIA AREA',
}

export enum REGIONS {
  AFRICA = 'Africa',
  AMERICAS = 'Americas',
  ASIA = 'Asia',
  EUROPE = 'Europe',
  OCEANIA = 'Oceania'
}

export const PAGE_SIZE = 10
export const START_PAGE = 0
