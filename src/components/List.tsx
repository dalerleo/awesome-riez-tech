import React from "react";
import Item from './Item'
import useCountriesContext from '../hooks/useCountriesContext'
import Pagination from "./Pagination";
const List = () => {
  const {
    countries,
    loading,
    currentPage,
    totalPages,
    onPage
  } = useCountriesContext()

  if (loading) {
    return <div>Loading...</div>
  }
  return (
    <div>
      {countries.length > 0
        ? countries.map((country) =>
        <Item key={country.name + country.region} {...country} />)
        : 'No data available'
      }
      <Pagination currentPage={currentPage} onPage={onPage} totalPages={totalPages}/>
    </div>
  )
}


export default React.memo(List)
