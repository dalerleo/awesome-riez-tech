import React from "react";
import {FILTER_STATE, ORDER_DIR} from "../constants";
import useCountriesContext from '../hooks/useCountriesContext'


const Filters = () => {
  const {order, onSort, filterState, onFilter} = useCountriesContext()

  return (
    <div className="filter-buttons">
      <div>
      {Object.values(FILTER_STATE).map((value) => (
        <button
          key={value}
          className={"filter " + (filterState === value ? 'filter-active': '')}
          onClick={() => onFilter(value)}>{value}
        </button>
      ))}
      </div>
      <button className="filter filter-active" onClick={onSort}>Sort {order === ORDER_DIR.ASC ? ' + ' : ' - '}</button>
    </div>
  )
}

export default Filters
