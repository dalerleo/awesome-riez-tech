import React from "react";
import {Country} from '../types'


const Item = (props: Country) => {
  const {area, region, name} = props

  return <div className="card">
    <p className="text">{name}</p>
    <div className="tags">
      <span className="tag">{region}</span>&nbsp;
      <span className="tag area">Area: {area}</span>
    </div>
  </div>
}

export default Item
