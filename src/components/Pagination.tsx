import React from "react";

type IPagination = {
  onPage: (page: number) => void,
  totalPages: number,
  currentPage: number
}
const Pagination = (props: IPagination) => {
const {onPage, totalPages, currentPage} = props
  return (
    <div className="pages">
      {
        Array(totalPages).fill(1).map((x,i) =>
          (<span
            key={i}
            className={"page " + ((i === currentPage) ? ' active-page': '')}
            onClick={() => onPage(i)} >
             {i + 1}
          </span>)
        )
      }
    </div>
  )
}

export default Pagination
