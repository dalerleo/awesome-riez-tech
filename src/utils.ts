import {Sort, GetPaginatedList, GetFilteredList, GetSortedList} from "./types";
import {FILTER_STATE, LITHUANIA_AREA, PAGE_SIZE, REGIONS, EMPTY_PAGINATED_LIST} from "./constants";

export const asc: Sort = (a, b) => a.name > b.name ? 1 : -1
export const desc: Sort = (a, b) => a.name < b.name ? 1 : -1

export const getSortedList: GetSortedList = (list, isAsc) => [...list].sort(
  (a,b) => {
    if (isAsc){
      return desc(a, b)
    }
    return asc(a, b)
  }
)

export const getFilteredList: GetFilteredList = (globalCountries, filter) => {
  if(filter === FILTER_STATE.OCEANIA_COUNTRIES){
    return globalCountries.filter(item => item.region === REGIONS.OCEANIA)
  }
  else if(filter === FILTER_STATE.SMALLER_LTH_AREA){
    return globalCountries.filter(item => item.area < LITHUANIA_AREA)
  }
  return [...globalCountries]

}

export const getPaginatedList: GetPaginatedList = (list) => {
  const arrayOfArrays = [];
  for (let i = 0; i < list.length; i += PAGE_SIZE) {
    arrayOfArrays.push(list.slice(i, i + PAGE_SIZE));
  }
  return arrayOfArrays.length > 0 ? arrayOfArrays : EMPTY_PAGINATED_LIST
}
