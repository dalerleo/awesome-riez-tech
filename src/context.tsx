import React, {createContext, ReactNode} from "react";
import useCountries from "./hooks/useCountries";
import {UseCountryData} from './types'

export const CountriesContext = createContext({} as UseCountryData)

export const CountriesContextProvider: React.FC<{children: ReactNode}> = ({children}) => {
  const value = useCountries()
  return (
    <CountriesContext.Provider value={value}>
      {children}
    </CountriesContext.Provider>
  )
}

