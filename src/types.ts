import {ORDER_DIR, FILTER_STATE} from "./constants";

export type Country = {
  name: string,
  region: String,
  area: number,
  independent: boolean
}

export type UseCountryData = {
  countries: Country[],
  loading: boolean,
  order: ORDER_DIR,
  currentPage: number,
  totalPages: number,
  filterState?: FILTER_STATE,
  onPage: (page: number) => void,
  onSort: () => void,
  onFilter: (filter: FILTER_STATE) => void,
}

export type GetSortedList = (list: Country[], isAsc?: boolean) => Country[]
export type GetFilteredList = (list: Country[], filterState: FILTER_STATE) => Country[]
export type GetPaginatedList = (list: Country[]) => Country[][]
export type UseCountries = () => UseCountryData

export type Sort = (a: Country, b: Country) => number

