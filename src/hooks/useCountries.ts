import React from "react";
import {Country, UseCountries} from "../types";
import {
  EMPTY_LIST,
  EMPTY_PAGINATED_LIST,
  FILTER_STATE,
  ORDER_DIR,
  PATH,
  START_PAGE
} from "../constants";
import axios from "axios";
import {getFilteredList, getPaginatedList, getSortedList} from '../utils'


type Params = {
  filterState: FILTER_STATE,
  order: ORDER_DIR,
  currentPage: number,
}

const initialParams = {
  filterState: FILTER_STATE.ALL,
  order: ORDER_DIR.ASC,
  currentPage: START_PAGE
}
const useCountries: UseCountries = () => {
  const [globalCountries, setGlobalCountries] = React.useState<Country[]>(EMPTY_LIST)
  const [paginatedCountries, setPaginatedCountries] = React.useState<Country[][]>(EMPTY_PAGINATED_LIST)
  const [countries, setCountries] = React.useState<Country[]>(EMPTY_LIST)
  const [loading, setLoading] = React.useState<boolean>(false)
  const [params, setParams] = React.useState<Params>(initialParams)

  React.useEffect(() => {
    const fetchCountries = async () => {
      setLoading(true)
      const {data} = await axios.get<Country[]>(PATH)
      setLoading(false)

      const list = getSortedList(data)
      const paginatedList = getPaginatedList(list)

      setGlobalCountries(list)
      setPaginatedCountries(paginatedList)
      setCountries(paginatedList[START_PAGE])
    }

    fetchCountries()
  }, [])


  const onFilter = (filterState: FILTER_STATE) => {
    const filteredList = getFilteredList(globalCountries, filterState)
    const paginatedList = getPaginatedList(filteredList)

    setParams({
      filterState,
      order: ORDER_DIR.ASC,
      currentPage: START_PAGE
    })
    setPaginatedCountries(paginatedList)
    setCountries(paginatedList[START_PAGE])
  }

  const onPage = (currentPage: number) => {
    setParams(prm => ({...prm, currentPage}))
    setCountries(paginatedCountries[currentPage])
  }

  const onSort = () => {
    const isAsc = params.order === ORDER_DIR.ASC
    const filteredList = getFilteredList(globalCountries, params.filterState)
    const sortedList = getSortedList(filteredList, isAsc)
    const paginatedSortList = getPaginatedList(sortedList)

    setParams(prm => ({
      ...prm,
      order: isAsc ? ORDER_DIR.DESC : ORDER_DIR.ASC,
      currentPage: START_PAGE
    }))
    setPaginatedCountries(getPaginatedList(sortedList))
    setCountries(paginatedSortList[START_PAGE])
  }

  const totalPages = paginatedCountries.length

  return {
    countries,
    loading,
    totalPages,
    ...params,
    onPage,
    onSort,
    onFilter
  }
}


export default useCountries
