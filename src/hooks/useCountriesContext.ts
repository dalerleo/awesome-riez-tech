import {useContext} from "react";
import {CountriesContext} from "../context";

const useCountriesContext = () => useContext(CountriesContext)
export default useCountriesContext
